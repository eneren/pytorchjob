
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader, Sampler, BatchSampler
import os
import h5py
from torch.utils import data



class HDF5Dataset(data.Dataset):
    def __init__(self, file_path, train_size, transform=None):
        super().__init__()
        self.file_path = file_path
        self.transform = transform
        self.hdf5file = h5py.File(self.file_path, 'r')
        
        if train_size > self.hdf5file['ecal']['layers'].shape[0]-1:
            self.train_size = self.hdf5file['ecal']['layers'].shape[0]-1
        else:
            self.train_size = train_size
            
        
    def __len__(self):
        return self.train_size
             
    def __getitem__(self, index):
        # get data
        x = self.get_data(index)
        if self.transform:
            x = torch.from_numpy(self.transform(x)).float()
        else:
            x = torch.from_numpy(x).float()
        e = torch.from_numpy(self.get_energy(index))
        if torch.sum(x) != torch.sum(x): #checks for NANs
            return self.__getitem__(int(np.random.rand()*self.__len__()))
        else:
            return x, e
    
    def get_data(self, i):
        return self.hdf5file['ecal']['layers'][i]
    
    def get_energy(self, i):
        return self.hdf5file['ecal']['energy'][i]


