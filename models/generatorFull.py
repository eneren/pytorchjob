import torch
import torch.nn as nn
import torch.nn.parallel
import torch.nn.functional as F



class Hcal_ecalEMB(nn.Module):
    """ 
        generator component of WGAN
    """
    def __init__(self, ngf, ndf, nz, emb_size=16):
        super(Hcal_ecalEMB, self).__init__()
        
       
        self.ndf = ndf
        self.emb_size = emb_size
        # ECAL component of convolutions
        # Designed for input 30*30*30
        
        self.conv_ECAL_1 = torch.nn.Conv3d(1, ndf, kernel_size=(2,2,2), stride=(1,1,1), padding=0, bias=False)
        self.ln_ECAL_1 = torch.nn.LayerNorm([29,29,29])
        self.conv_ECAL_2 = torch.nn.Conv3d(ndf, ndf, kernel_size=2, stride=(2,2,2), padding=0, bias=False)
        self.ln_ECAL_2 = torch.nn.LayerNorm([14,14,14])
        self.conv_ECAL_3 = torch.nn.Conv3d(ndf, ndf, kernel_size=4, stride=(2,2,2), padding=(1,1,1), bias=False)
        
        self.conv_lin_ECAL = torch.nn.Linear(7*7*7*ndf, 64) 
        
        self.econd_lin = torch.nn.Linear(1, 64) # label embedding

        self.fc1 = torch.nn.Linear(64*2, 128)  # 2 components after cat
        self.fc2 = torch.nn.Linear(128,  64)
        self.fc3 = torch.nn.Linear(64, emb_size)
        
        
        ## HCAL component of convolutions
        self.ngf = ngf
        self.nz = nz
        
        kernel = 4
        

        self.conv1 = nn.ConvTranspose3d(emb_size + nz + 1, ngf, kernel, 1, 0, bias=False)
        ##torch.Size([100, 64, 4, 4, 4])
    
        
        # outs from first convolutions concatenate state size [ ngf*8 x 4 x 4]
        # and going into main convolutional part of Generator
        self.main_conv = nn.Sequential(
            
            nn.ConvTranspose3d(ngf, ngf*4, kernel_size=(4,2,2), stride=2, padding=1, bias=False),
            nn.LayerNorm([8, 6, 6]),
            nn.ReLU(),
            # state shape [ (ndf*4) x 6 x 6 ]

            nn.ConvTranspose3d(ngf*4, ngf*2, kernel_size=(4,2,2), stride=2, padding=1, bias=False),
            nn.LayerNorm([16, 10, 10]),
            nn.ReLU(),
            # state shape [ (ndf*2) x 10 x 10 ]

            nn.ConvTranspose3d(ngf*2, ngf, kernel_size=(4,4,4), stride=(2,1,1), padding=1, bias=False),
            nn.LayerNorm([32, 11, 11]),
            nn.ReLU(),
            # state shape [ (ndf) x 11 x 11 ]

            nn.ConvTranspose3d(ngf, ngf, kernel_size=(10,4,4), stride=1, padding=1, bias=False),
            nn.LayerNorm([39, 12, 12]),
            nn.ReLU(),
            # state shape [ ch=10 x 12 x 12 ]
           
            nn.ConvTranspose3d(ngf, 5, kernel_size=(8,3,3), stride=(1,2,2), padding=1, bias=False),
            nn.LayerNorm([44, 23, 23]),
            nn.ReLU(),
            
            # state shape [ ch=5 x 23 x 23  ]
            
            nn.ConvTranspose3d(5, 1, kernel_size=(7,10,10), stride=1, padding=1, bias=False),
            nn.ReLU()
            
            ## final output ---> [48 x 25 x 25]
        )

    def forward(self, noise, energy, img_ECAL):
        
        
        batch_size = img_ECAL.size(0)
        # input: img_ECAL = [batch_size, 1, 30, 30, 30]
        #        
        
        # ECAL 
        x_ECAL = F.leaky_relu(self.ln_ECAL_1(self.conv_ECAL_1(img_ECAL)), 0.2)
        x_ECAL = F.leaky_relu(self.ln_ECAL_2(self.conv_ECAL_2(x_ECAL)), 0.2)
        x_ECAL = F.leaky_relu(self.conv_ECAL_3(x_ECAL), 0.2)
        x_ECAL = x_ECAL.view(-1, self.ndf*7*7*7)
        x_ECAL = F.leaky_relu(self.conv_lin_ECAL(x_ECAL), 0.2)
        
        x_E = F.leaky_relu(self.econd_lin(energy), 0.2)
        
        xa = torch.cat((x_ECAL, x_E), 1)
        
        xa = F.leaky_relu(self.fc1(xa), 0.2)
        xa = F.leaky_relu(self.fc2(xa), 0.2)
        xa = self.fc3(xa)
        
        
        xzm = torch.cat((xa, energy, noise), 1) 
        xzm = xzm.view(xzm.size(0), xzm.size(1), 1, 1, 1)
            
    
        inpt = self.conv1(xzm)
             
        x = self.main_conv(inpt)
        
        x = x.view(-1, 48, 30, 30)
        
        return x
