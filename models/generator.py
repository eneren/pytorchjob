
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils import data


class DCGAN_G(nn.Module):
    """ 
        generator component of WGAN
    """
    def __init__(self, ngf, nz):
        super(DCGAN_G, self).__init__()
        
        self.ngf = ngf
        self.nz = nz

        kernel = 4
        
        # input energy shape [batch x 1 x 1 x 1 ] going into convolutional
        self.conv1_1 = nn.ConvTranspose3d(1, ngf*4, kernel, 1, 0, bias=False)
        # state size [ ngf*4 x 4 x 4 x 4]
        
        # input noise shape [batch x nz x 1 x 1] going into convolutional
        self.conv1_100 = nn.ConvTranspose3d(nz, ngf*4, kernel, 1, 0, bias=False)
        # state size [ ngf*4 x 4 x 4 x 4]
        
        
        # outs from first convolutions concatenate state size [ ngf*8 x 4 x 4]
        # and going into main convolutional part of Generator
        self.main_conv = nn.Sequential(
            
            nn.ConvTranspose3d(ngf*8, ngf*4, kernel, 2, 1, bias=False),
            nn.LayerNorm([8, 8, 8]),
            nn.ReLU(),
            # state shape [ (ndf*4) x 8 x 8 ]

            nn.ConvTranspose3d(ngf*4, ngf*2, kernel, 2, 1, bias=False),
            nn.LayerNorm([16, 16, 16]),
            nn.ReLU(),
            # state shape [ (ndf*2) x 16 x 16 ]

            nn.ConvTranspose3d(ngf*2, ngf, kernel, 2, 1, bias=False),
            nn.LayerNorm([32, 32, 32]),
            nn.ReLU(),
            # state shape [ (ndf) x 32 x 32 ]

            nn.ConvTranspose3d(ngf, 1, 3, 1, 2, bias=False),
            nn.ReLU()
            # state shape [ 1 x 30 x 30 x 30 ]
        )

    def forward(self, noise, energy):
        energy_trans = self.conv1_1(energy)
        noise_trans = self.conv1_100(noise)
        input = torch.cat((energy_trans, noise_trans), 1)
        x = self.main_conv(input)
        x = x.view(-1, 30, 30, 30)
        return x


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('LayerNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)
    elif classname.find('Linear') != -1:
        m.weight.data.normal_(0.0, 0.02)
        m.bias.data.fill_(0)
