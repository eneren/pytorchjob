from __future__ import print_function
from comet_ml import Experiment
import argparse
import os, sys
import numpy as np
import torch
import torch.distributed as dist
import torch.nn as nn
import torch.optim as optim
from torch import autograd
from torch.utils.data.distributed import DistributedSampler
from torch.utils.data import DataLoader
from torch.autograd import Variable


os.environ['MKL_THREADING_LAYER'] = 'GNU'

torch.autograd.set_detect_anomaly(True)

sys.path.append('/opt/regressor/src')

from models.generator import DCGAN_G
#from models.constrainer import energyRegressor, weights_init
from models.constrainer import weights_init
from models.data_loader import HDF5Dataset
from models.criticRes import generate_model
os.system("python models/criticRes.py")



def calc_gradient_penalty(netD, real_data, fake_data, real_label, BATCH_SIZE, device, layer, xsize, ysize):
    
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement()/BATCH_SIZE)).contiguous()
    alpha = alpha.view(BATCH_SIZE, 1, layer, xsize, ysize)
    alpha = alpha.to(device)


    fake_data = fake_data.view(BATCH_SIZE, 1, layer, xsize, ysize)
    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)   

    disc_interpolates = netD(interpolates.float(), real_label.float())


    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)                              
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean()
    return gradient_penalty

    
def train(args, aD, aG, device, train_loader, optimizer_d, optimizer_g, epoch, experiment):
    
    ### CRITIC TRAINING
    aD.train()
    aG.eval()

    Tensor = torch.cuda.FloatTensor 
   
    for batch_idx, (data, energy) in enumerate(train_loader):
        real_data = data.to(device).unsqueeze(1)
        real_label = energy.to(device)
        
        optimizer_d.zero_grad()
        
        z = Variable(Tensor(np.random.uniform(-1, 1, (args.batch_size, args.nz, 1, 1, 1))))

        fake_data = aG(z, real_label.view(-1, 1, 1, 1, 1) ).detach()        

        ## get critic score from real data
        disc_real = aD(real_data.float(), real_label.float()) 

        ## Calculate Gradient Penalty Term
        gradient_penalty = calc_gradient_penalty(aD, real_data.float(), fake_data, real_label, args.batch_size, device, layer=30, xsize=30, ysize=30)

        fake_data = fake_data.unsqueeze(1)  ## transform to [BS, 1, 30, 30, 30]
        disc_fake = aD(fake_data, real_label.float())


        ## wasserstein-1 distace
        w_dist = torch.mean(disc_fake) - torch.mean(disc_real)
        # final disc cost
        disc_cost = w_dist + args.lambd * gradient_penalty



        disc_cost.backward()
        optimizer_d.step()

        if batch_idx % args.log_interval == 0:
            print('Critic --> Train Epoch: {} [{}/{} ({:.0f}%)]\tloss={:.4f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), disc_cost.item()))
            niter = epoch * len(train_loader) + batch_idx
            experiment.log_metric("L_crit", disc_cost, step=niter)
            experiment.log_metric("gradient_pen", gradient_penalty, step=niter)
            experiment.log_metric("Wasserstein Dist", w_dist, step=niter)
            experiment.log_metric("Critic Score (Real)", torch.mean(disc_real), step=niter)
            experiment.log_metric("Critic Score (Fake)", torch.mean(disc_fake), step=niter)

        
        ## training generator per ncrit 
        if (batch_idx % args.ncrit == 0) and (batch_idx != 0):
            ## GENERATOR TRAINING
            aD.eval()
            aG.train()
            #aE.eval()
            
            #print("Generator training started")

            optimizer_g.zero_grad()
    

            ## generate fake data out of noise
            fake_dataG = aG(z, real_label.view(-1, 1, 1, 1, 1).float())
             
                    
            #output_g = aE(fake_dataG)  ## transform to [BS, layer, x, y]

            #real_data = real_data.squeeze(1)  ## transform to [BS, layer, x, y]
            #output_r = aE(real_data.float())


            #aux_fake = (output_g - real_label)**2
            #aux_real = (output_r - real_label)**2
            
            #aux_errG = torch.abs(aux_fake - aux_real)
            
            ## Total loss function for generator
            gen_cost = aD(fake_dataG.unsqueeze(1).float(), real_label.float())
            #g_cost = -torch.mean(gen_cost) + args.kappa*torch.mean(aux_errG) 
            g_cost = -torch.mean(gen_cost) 
            g_cost.backward()
            optimizer_g.step()

            if batch_idx % args.log_interval == 0 :
                print('Generator --> Train Epoch: {} [{}/{} ({:.0f}%)]\tloss={:.4f}'.format(
                    epoch, batch_idx * len(data), len(train_loader.dataset),
                    100. * batch_idx / len(train_loader), g_cost.item()))
                niter = epoch * len(train_loader) + batch_idx
                experiment.log_metric("L_Gen", g_cost, step=niter)
                #experiment.log_metric("L_Gen", torch.mean(gen_cost), step=niter)



def is_distributed():
    return dist.is_available() and dist.is_initialized()


def parse_args():
    parser = argparse.ArgumentParser(description='WGAN training on hadron showers')
    parser.add_argument('--batch-size', type=int, default=100, metavar='N',
                        help='input batch size for training (default: 100)')
    
    parser.add_argument('--nz', type=int, default=100, metavar='N',
                        help='latent space for generator (default: 100)')
    
    parser.add_argument('--lambd', type=int, default=15, metavar='N',
                        help='weight of gradient penalty  (default: 15)')

    parser.add_argument('--kappa', type=float, default=0.001, metavar='N',
                        help='weight of label conditioning  (default: 0.001)')

    parser.add_argument('--dres', type=int, default=34, metavar='N',
                        help='depth of Residual critic (default: 34)')

    parser.add_argument('--ngf', type=int, default=32, metavar='N',
                        help='n-feature of generator  (default: 32)')

    parser.add_argument('--ncrit', type=int, default=10, metavar='N',
                        help='critic updates before generator one  (default: 10)')

    parser.add_argument('--epochs', type=int, default=1, metavar='N',
                        help='number of epochs to train (default: 1)')
    parser.add_argument('--lrCrit', type=float, default=0.00001, metavar='LR',
                        help='learning rate Critic (default: 0.00001)')
    parser.add_argument('--lrGen', type=float, default=0.0001, metavar='LR',
                        help='learning rate Generator (default: 0.0001)')

    parser.add_argument('--chpt', action='store_true', default=False,
                        help='continue training from a saved model')

    parser.add_argument('--chpt_base', type=str, default='/eos/user/e/eneren/experiments/',
                        help='continue training from a saved model')

    parser.add_argument('--exp', type=str, default='dist_wgan',
                        help='name of the experiment')

    parser.add_argument('--chpt_eph', type=int, default=1,
                        help='continue checkpoint epoch')

    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=100, metavar='N',
                        help='how many batches to wait before logging training status')
   

    if dist.is_available():
        parser.add_argument('--backend', type=str, help='Distributed backend',
                            choices=[dist.Backend.GLOO, dist.Backend.NCCL, dist.Backend.MPI],
                            default=dist.Backend.GLOO)
    
    parser.add_argument('--local_rank', type=int, default=0)

    args = parser.parse_args()


    args.local_rank = int(os.environ.get('LOCAL_RANK', args.local_rank))
    args.rank = int(os.environ.get('RANK'))
    args.world_size = int(os.environ.get('WORLD_SIZE'))


    # postprocess args
    args.device = f'cuda:{args.local_rank}'  # PytorchJob/launch.py
    args.batch_size = max(args.batch_size,
                          args.world_size * 2)  # min valid batchsize
    return args


def run(args):
    # Training settings

    use_cuda = not args.no_cuda and torch.cuda.is_available()
    if use_cuda:
        print('Using CUDA')

    
    experiment = Experiment(api_key="keGmeIz4GfKlQZlOP6cit4QOi",
                        project_name="ecal-hcal-shower", workspace="engineren", auto_output_logging="simple")
    experiment.add_tag(args.exp)

    experiment.log_parameters(
        {
        "batch_size" : args.batch_size,
        "latent": args.nz,
        "lambda": args.lambd,
        "ncrit" : args.ncrit,
        "resN": args.dres,
        "ngf": args.ngf
        }
    )

    torch.manual_seed(args.seed)

    device = torch.device("cuda" if use_cuda else "cpu")
    


    print('Using distributed PyTorch with {} backend'.format(args.backend))
    dist.init_process_group(backend=args.backend)

    print('[init] == local rank: {}, global rank: {}, world size: {} =='.format(args.local_rank, args.rank, args.world_size))



    print ("loading data")
    #dataset = HDF5Dataset('/eos/user/e/eneren/scratch/40GeV40k.hdf5', transform=None, train_size=40000)
    #dataset = HDF5Dataset('/eos/user/e/eneren/scratch/60GeV20k.hdf5', transform=None, train_size=20000)
    dataset = HDF5Dataset('/eos/user/e/eneren/scratch/4060GeV.hdf5', transform=None, train_size=60000)


    sampler = DistributedSampler(dataset, shuffle=True)    
    train_loader = DataLoader(dataset, batch_size=args.batch_size, sampler=sampler, num_workers=1, drop_last=True, pin_memory=False)



    mCrit = generate_model(args.dres).to(device)
    mGen = DCGAN_G(args.ngf, args.nz).to(device)
    #mReg = energyRegressor().to(device)


    if args.world_size > 1: 
        Distributor = nn.parallel.DistributedDataParallel if use_cuda \
            else nn.parallel.DistributedDataParallelCPU
        #mReg = Distributor(mReg, device_ids=[args.local_rank], output_device=args.local_rank, find_unused_parameters=True )
        mCrit = Distributor(mCrit, device_ids=[args.local_rank], output_device=args.local_rank )
        mGen = Distributor(mGen, device_ids=[args.local_rank], output_device=args.local_rank)

    
    optimizerG = optim.Adam(mGen.parameters(), lr=args.lrGen, betas=(0.5, 0.9))
    optimizerD = optim.Adam(mCrit.parameters(), lr=args.lrCrit, betas=(0.5, 0.9))

    if (args.chpt):
        critic_checkpoint = torch.load(args.chpt_base + args.exp + "_critic_"+ str(args.chpt_eph) + ".pt")
        gen_checkpoint = torch.load(args.chpt_base + args.exp + "_generator_"+ str(args.chpt_eph) + ".pt")
        
        mGen.load_state_dict(gen_checkpoint['model_state_dict'])
        optimizerG.load_state_dict(gen_checkpoint['optimizer_state_dict'])

        mCrit.load_state_dict(critic_checkpoint['model_state_dict'])
        optimizerD.load_state_dict(critic_checkpoint['optimizer_state_dict'])
        
        eph = gen_checkpoint['epoch']
    
    else: 
        eph = 0
        print ("init models")
        mCrit.apply(weights_init)
        mGen.apply(weights_init)

    #reg_checkpoint = torch.load(args.chpt_base + "dist_launch_sampler_regressor_2.pt")
    #mReg.load_state_dict(reg_checkpoint['model_state_dict'])

    
    experiment.set_model_graph(str(mGen), overwrite=False)

    print ("starting training...")
    for epoch in range(1, args.epochs + 1):
        epoch += eph
        train_loader.sampler.set_epoch(epoch)
        train(args, mCrit, mGen, device, train_loader, optimizerD, optimizerG, epoch, experiment)
        if args.rank == 0:
            gPATH = args.chpt_base + args.exp + "_generator_"+ str(epoch) + ".pt"
            cPATH = args.chpt_base + args.exp + "_critic_"+ str(epoch) + ".pt"
            torch.save({
                'epoch': epoch,
                'model_state_dict': mGen.state_dict(),
                'optimizer_state_dict': optimizerG.state_dict()
                }, gPATH)
            
            torch.save({
                'epoch': epoch,
                'model_state_dict': mCrit.state_dict(),
                'optimizer_state_dict': optimizerD.state_dict()
                }, cPATH)


    print ("end training")
        


def main():
    args = parse_args()
    run(args)
            
if __name__ == '__main__':
    main()

