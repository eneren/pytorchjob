from __future__ import print_function

import argparse
import os
import sys
import torch
import torch.distributed as dist
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data.distributed import DistributedSampler
from torch.utils.data import DataLoader


print("whoami: ", os.getuid())


sys.path.append('/opt/regressor/src')


from models.constrainer import energyRegressor, weights_init
from models.data_loader import HDF5Dataset

WORLD_SIZE = int(os.environ.get('WORLD_SIZE', 1))




    
def train(args, aE, device, train_loader, optimizer_e, epoch):
    
    ### Energy regressor TRAINING
    aE.train()

    for batch_idx, (data, energy) in enumerate(train_loader):
        real_data = data.to(device)
        real_label = energy.to(device)
        
        optimizer_e.zero_grad()
        output = aE(real_data.float()) 

        #print (output.type(), real_label.view(len(real_data), 1).type())
        

        eLoss = F.mse_loss(output, real_label.view(len(real_data), 1).float())
        eLoss.backward()
        optimizer_e.step()

        if batch_idx % args.log_interval == 0:
            print('Energy Regressor --> Train Epoch: {} [{}/{} ({:.0f}%)]\tloss={:.4f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), eLoss.item()))
            

        
def test(args, aE, device, test_loader, epoch):
    aE.eval()
    
    with torch.no_grad():
        for batch_idx, (data, target) in enumerate(test_loader):
            data, target = data.to(device), target.to(device)
            output = aE(data)
            testLoss = F.mse_loss(output, target.view(len(data), 1))
            if batch_idx % args.log_interval == 0:
                print('Energy Regressor --> Test Epoch: {} [{}/{} ({:.0f}%)]\tloss={:.4f}'.format(
                    epoch, batch_idx * len(data), len(test_loader.dataset),
                    100. * batch_idx / len(test_loader), testLoss.item()))



def should_distribute():
    return dist.is_available() and WORLD_SIZE > 1


def is_distributed():
    return dist.is_available() and dist.is_initialized()


def cleanup():
    dist.destroy_process_group()

def parse_args():
    # Training settings
    parser = argparse.ArgumentParser(description='Energy Regressor Training')
    parser.add_argument('--batch-size', type=int, default=50, metavar='N',
                        help='input batch size for training (default: 100)')


    parser.add_argument('--epochs', type=int, default=1, metavar='N',
                        help='number of epochs to train (default: 1)')
    parser.add_argument('--lr', type=float, default=0.0001, metavar='LR',
                        help='learning rate regressor (default: 0.00001)')

    parser.add_argument('--chpt', action='store_true', default=False,
                        help='continue training from a saved model')

    parser.add_argument('--chpt_base', type=str, default='/eos/user/e/eneren/experiments/',
                        help='continue training from a saved model')

    parser.add_argument('--exp', type=str, default='dist_pytorch_lazyload',
                        help='name of the experiment')

    parser.add_argument('--chpt_eph', type=int, default=1,
                        help='continue checkpoint epoch')


    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=27, metavar='S',
                        help='random seed (default: 27)')
    parser.add_argument('--log-interval', type=int, default=100, metavar='N',
                        help='how many batches to wait before logging training status')

    if dist.is_available():
        parser.add_argument('--backend', type=str, help='Distributed backend',
                            choices=[dist.Backend.GLOO, dist.Backend.NCCL, dist.Backend.MPI],
                            default=dist.Backend.GLOO)                        
    
    parser.add_argument('--local_rank', type=int, default=0)

    args = parser.parse_args()

    args.local_rank = int(os.environ.get('LOCAL_RANK', args.local_rank))
    args.rank = int(os.environ.get('RANK'))
    args.world_size = int(os.environ.get('WORLD_SIZE'))


    # postprocess args
    args.device = f'cuda:{args.local_rank}'  # PytorchJob/launch.py
    args.batch_size = max(args.batch_size,
                          args.world_size * 2)  # min valid batchsize
    return args


def run(args):
    print ("beginning of python script")
    
       

    use_cuda = not args.no_cuda and torch.cuda.is_available()

    print(torch.cuda.is_available())

    if use_cuda:
        print('Using CUDA')



    torch.manual_seed(args.seed)

    device = torch.device("cuda" if use_cuda else "cpu")

    #print(device)
    #print (should_distribute())

    
    print('Using distributed PyTorch with {} backend'.format(args.backend))
    dist.init_process_group(backend=args.backend)

    print('[init] == local rank: {}, global rank: {}, world size: {} =='.format(args.local_rank, args.rank, args.world_size))



    print ("loading data")
    dataset = HDF5Dataset('/eos/user/e/eneren/run_prod75k/hdf5/training_75k.hdf5', transform=None, train_size=75000)
    testset = HDF5Dataset('/eos/user/e/eneren/run_prod3k/validation3k.hdf5', transform=None, train_size=3000)

    loader_params = {'shuffle': True, 'num_workers': 1, 'pin_memory': True} if use_cuda else {}

    
    sampler = DistributedSampler(dataset, shuffle=True)    
    train_loader = DataLoader(dataset, batch_size=args.batch_size, sampler=sampler, num_workers=1, pin_memory=False)

    #train_loader = torch.utils.data.DataLoader(dataset, batch_size = args.batch_size, drop_last=True, **loader_params)
    #train_loader = fast_loader(dataset, batch_size=args.batch_size, drop_last=True)

    test_loader = torch.utils.data.DataLoader(testset, batch_size = args.batch_size, drop_last=True, **loader_params)

    mReg = energyRegressor().to(args.device)

    optimizerE = optim.Adam(mReg.parameters(), lr=args.lr, betas=(0.5, 0.9))
    
    if args.world_size > 1: 
        Distributor = nn.parallel.DistributedDataParallel if use_cuda \
            else nn.parallel.DistributedDataParallelCPU
        mReg = Distributor(mReg, device_ids=[args.local_rank], output_device=args.local_rank, find_unused_parameters=True )


    if (args.chpt):
        checkpoint = torch.load(args.chpt_base + args.exp + "_regressor_"+ str(args.chpt_eph) + ".pt")
        mReg.load_state_dict(checkpoint['model_state_dict'])
        optimizerE.load_state_dict(checkpoint['optimizer_state_dict'])
        eph = checkpoint['epoch']
    else: 
        eph = 0
        print ("init models")
        mReg.apply(weights_init)




    print ("starting training...")
    for epoch in range(1, args.epochs + 1):
        epoch += eph 

        train_loader.sampler.set_epoch(epoch)

        train(args, mReg, device, train_loader, optimizerE, epoch)
        if args.rank == 0:
            test(args, mReg, device, test_loader, epoch)
            PATH = args.chpt_base + args.exp + "_regressor_"+ str(epoch) + ".pt"
            torch.save({
                'epoch': epoch,
                'model_state_dict': mReg.state_dict(),
                'optimizer_state_dict': optimizerE.state_dict()
                }, PATH)

    
    print ("end training")
    
    
def main():
    args = parse_args()
    run(args)
            
if __name__ == '__main__':
    main()

